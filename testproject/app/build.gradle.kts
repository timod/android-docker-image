plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}

val composeCompilerVersion = "1.4.7"

android {
    namespace = "com.example.testapp"
    buildToolsVersion = "33.0.2"
    compileSdk = 33

    defaultConfig {
        applicationId = "com.example.testapp"
        minSdk = 26
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    kotlinOptions {
        jvmTarget = "11"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = composeCompilerVersion
    }
    packagingOptions {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {
    implementation("androidx.activity:activity-compose:1.7.2")
    val composeBom = platform("androidx.compose:compose-bom:2023.06.00")
    implementation(composeBom)
    androidTestImplementation(composeBom)
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.ui:ui-tooling-preview:")
    implementation("androidx.compose.material:material")
    implementation("androidx.compose.material:material-icons-extended:")
    debugImplementation("androidx.compose.ui:ui-tooling:")
    debugImplementation("androidx.compose.ui:ui-test-manifest:")

    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    androidTestImplementation("androidx.compose.ui:ui-test-junit4")
}