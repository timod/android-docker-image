FROM bellsoft/liberica-openjdk-alpine:17

ARG ANDROID_SDK_TOOLS
ARG ANDROID_COMPILE_SDK
ARG ANDROID_BUILD_TOOLS

ENV ANDROID_HOME "/opt/sdk"
ENV PATH=$PATH:${ANDROID_HOME}/cmdline-tools/bin/

RUN apk upgrade &&\
    apk add --no-cache bash curl git wget tar unzip coreutils &&\
    rm -rf /tmp/* && \
    rm -rf /var/cache/apk/*

RUN mkdir $ANDROID_HOME && \
    wget -q --output-document=$ANDROID_HOME/cmdline-tools.zip "https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip" &&\
    unzip -d $ANDROID_HOME $ANDROID_HOME/cmdline-tools.zip &&\
    rm -v $ANDROID_HOME/cmdline-tools.zip

RUN sdkmanager --sdk_root=${ANDROID_HOME} --version &&\
    yes | sdkmanager --sdk_root=${ANDROID_HOME} --licenses || true &&\
    sdkmanager --sdk_root=${ANDROID_HOME} "platforms;android-${ANDROID_COMPILE_SDK}" &&\
    sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools" &&\
    sdkmanager --sdk_root=${ANDROID_HOME} "build-tools;${ANDROID_BUILD_TOOLS}"
