# Android Docker Image

This project generates docker images for the CI to build Android apps.

## Gitlab docker registry

Every project in gitlab do have a container registry where it is possible to upload docker images.
The registry for this project is here:
https://gitlab.com/timod/android-docker-image/container_registry
The version of the images is derived from the android build tools version number.

## Using the docker image in other projects

Add the following image entry in the .gitlab-ci.yml file:
(Change the name to correct build tools version 33.0.2 is for build tools version 33.0.2)
```yml
image: registry.gitlab.com/timod/android-docker-image:33.0.2

build:debug:
  stage: build
  script:
    - ./gradlew assembleDebug
```

## Creating image for different Android Build Tools version

- change the .gitlab-ci.yml file to
- Change following variables
```
ENV ANDROID_COMPILE_SDK "33"
ENV ANDROID_BUILD_TOOLS "33.0.2"
ENV ANDROID_SDK_TOOLS "9123335"
```
Current ANDROID_SDK_TOOLS must be looked up on the
https://developer.android.com/studio/index.html webpage at the
"Command line tools only" section.

To upload the new image to the registry just execute the CI job.
